module com.example.autemo {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.autemo to javafx.fxml;
    exports com.example.autemo;
}